window.onload=function(e) {
    let runner = function (maze) {
        showMaze(maze, '');

        let [playerX, playerY] = getRandomPlace(maze);
        let [exitX,   exitY]   = getRandomPlace(maze);

        maze[exitY][exitX] = 'G';

        showStart(playerX,  playerY, '');
        showExit(exitX,     exitY,   '');

        console.log(maze);

        player = new Player(maze, playerX, playerY);

        let redrawDebug = function(known, startX, startY) {
            redrawKnown(known, startX, startY);
        }

        let stepDebug = function(player) {
            redrawPlayer(player, '');
        };

        seek(player, redrawDebug, stepDebug).then((result) => console.log(result));
    }

    loadMaze(runner);
}

function getRandomPlace(maze) {
    var sizeX = maze.length;
    var sizeY = maze[0].length;

    for (let step = 0; step < 100; step++) {
        var X = Math.floor(Math.random() * sizeX);
        var Y = Math.floor(Math.random() * sizeX);

        if (maze[Y][X] == 'E') {
            return [X, Y];
        }
    }

    console.log('Unable to find empty place!');

    return [0,0];
}
