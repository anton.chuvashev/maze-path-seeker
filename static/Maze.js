function loadMaze(cb) {
    var client = new XMLHttpRequest();

    var maze = [];

    client.open('GET', '/maze3.txt');

    client.onloadend = function() {
        var text = client.responseText;
        var lines = text.split('\n');

        lines.forEach((l) => l.length && maze.push(l.split('')));

        cb(maze);
    }

    client.send();
}
