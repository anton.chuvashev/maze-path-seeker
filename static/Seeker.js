class Process {
    constructor(player) {
        this.player         = player;
        this.cur_x          = 2;
        this.cur_y          = 2;
        this.new_x          = null;
        this.new_y          = null;
        this.start_x        = 2;
        this.start_y        = 2;
        this.steps_count    = 0;
        this.allowed_routes = {};
        this.useless_points = {};
        this.is_wall_top    = 0;
        this.is_wall_bottom = 0;
        this.is_wall_left   = 0;
        this.is_wall_right  = 0;

        this.setMaze(lookAround(player));
    }

    countSteps(number) {
        this.steps_count += number;
    }

    setMaze(maze) {
        this.maze   = maze;
        this.maze_x = maze[0].length;
        this.maze_y = maze.length;
    }

    getAllowedRoutes() {
        return this.allowed_routes;
    }

    setRouteAllowedTo(x, y) {
        this.allowed_routes[y]    ||= {};
        this.allowed_routes[y][x] ||= 1;
    }

    isRouteAllowedTo(x, y) {
        return this.allowed_routes[y] && this.allowed_routes[y][x];
    }

    dropRoute(x, y) {
        if (this.allowed_routes[y] && this.allowed_routes[y][x]) {
            delete this.allowed_routes[y][x];
        }
    }

    setAllowedRoutes(routes) {
        this.allowed_routes = routes;
    }
}

class Position {
    constructor(x, y, cmd) {
        this.x           = x;
        this.y           = y;
        this.path_length = 0;
        this.parent      = null;
        this.path_left   = 0;
        this.path_total  = 0;
        this.cmd         = cmd;
    }
}

async function seek(player, redraw_debug, stepDebug) {
    let process = new Process(player);
    let road    = [];

    let real_start_x = player.x;
    let real_start_y = player.y;

    for (let step = 0; step < 10000; step++) {
        let route = getTarget(process);

        for (let i = 0; i < route.length; i++) {
            let cmd = route[i];

            stepPlayer(player, cmd);

            if (stepDebug) {
                stepDebug(player);
                await sleep();
            }
        }

        addLook(process);

        process.countSteps(route.length + 1);

        if (redraw_debug) {
            redraw_debug(process.maze, process.start_x, process.start_y);
        }

        if (process.is_finish) {
            road = getRoute(
                process,
                [process.start_x, process.start_y],
                [process.cur_x, process.cur_y],
            );

            showPath(real_start_x, real_start_y, road, '');
            showPath(process.start_x, process.start_y, road, '-known');
            showExit(process.cur_x, process.cur_y, '-known');

            console.log(process.steps_count);
            break;
        }
    }

    return road;
}

function getTarget(process) {
    let all_routes = [];

    let [finish_x, finish_y] = [-1, -1];

    loopG:
    for (let y = 0; y < process.maze_y; y++) {
        for (let x = 0; x < process.maze_x; x++) {
            if (process.maze[y][x] === 'G') {
                [finish_x, finish_y] = [x, y];
                break loopG;
            }
        }
    }

    loopRate:
    for (let y = 0; y < process.maze.length; y++) {
        for (let x = 0; x < process.maze[0].length; x++) {

            if (process.cur_x === x && process.cur_y === y) {
                continue;
            }

            if (process.useless_points[y] && process.useless_points[y][x] === 1) {
                continue;
            }

            if (!['E','G'].includes(process.maze[y][x])) {
                continue;
            }

            let route = getRoute(process, [process.cur_x, process.cur_y], [x, y]);

            if (route) {
                if (process.maze[y][x] === 'G') {
                    process.new_x = x;
                    process.new_y = y;

                    process.is_finish = 1;

                    all_routes = [
                        {
                            rate:  100500,
                            route: route,
                            new_x: x,
                            new_y: y,
                        }
                    ];

                    break loopRate;
                }

                if (route.length) {
                    let rate = getRate(process, x, y, route.length, finish_x, finish_y);

                    all_routes.push(
                        {
                            rate:  rate,
                            route: route,
                            new_x: x,
                            new_y: y,
                        }
                    );
                }
            }
        }
    }

    let selected_rate   = all_routes.sort((a, b) => (b.rate - a.rate))[0]["rate"];
    let selected_routes = all_routes.filter((route) => (route["rate"] === selected_rate));

    let selected_route = selected_routes[0];

    process.new_x = selected_route["new_x"];
    process.new_y = selected_route["new_y"];

    return selected_route["route"];
}

function getRate(process, x, y, route_length, finish_x, finish_y) {
    let cells_to_unveil = 0;

    for (let cy = y - 2; cy <= y + 2; cy++) {
        for (let cx = x - 2; cx <= x + 2; cx++) {
            if (
                !process.maze[cy]
                || !process.maze[cy][cx]
                || process.maze[cy][cx] === "U"
            ) {
                cells_to_unveil++;
            }
        }
    }

    let range = 7;

    if (cells_to_unveil) {
        for (let cy = y - range; cy <= y + range; cy++) {
            for (let cx = x - range; cx <= x + range; cx++) {
                if (process.maze[cy] && process.maze[cy][cx] === 'U') {
                    cells_to_unveil += 0.01;
                }
            }
        }

        if (cells_to_unveil < 1) {
            cells_to_unveil = 1;
        }
    }

    let rate = 0;

    if (cells_to_unveil) {
        rate = cells_to_unveil / route_length;
    }

    let cur_x = process.cur_x;
    let cur_y = process.cur_y;
    let direction_index = 1;


    if (process.maze[0][3] === 'X') {
        process.is_wall_top = 1;
    }

    if (process.maze[process.maze_y - 1][3] === 'X') {
        process.is_wall_bottom = 1;
    }

    if (process.maze[3][0] === 'X') {
        process.is_wall_left = 1;
    }

    if (process.maze[3][process.maze_x - 1] === 'X') {
        process.is_wall_right = 1;
    }

    if (
           !process.is_wall_left === 1 && cur_x > x
        || !process.is_wall_right === 1 && cur_x < x
    ) {
        rate *= direction_index * Math.abs(cur_x - x);
    }

    if (
           !process.is_wall_top === 1 && cur_y < y
        || !process.is_wall_bottom === 1 && cur_y > y
    ) {
        rate *= direction_index * Math.abs(cur_y - y);
    }

    return rate;
}

function addLook(process) {
    let look = lookAround(process.player);

    let new_x   = process.new_x;
    let new_y   = process.new_y;
    let start_x = process.start_x;
    let start_y = process.start_y;

    let size_y = process.maze_y;
    let size_x = process.maze_x;

    if (new_x < 2             ) {size_x += 2     - new_x}
    if (new_x > size_x - 1 - 2) {size_x += new_x - size_x + 1 + 2}

    if (new_y < 2             ) {size_y += 2     - new_y}
    if (new_y > size_y - 1 - 2) {size_y += new_y - size_y + 1 + 2}

    let result = Array(size_y).fill(0).map(() => Array(size_x).fill('U'));

    let known_delta_x = 0;
    let known_delta_y = 0;

    if (new_x < 2) {known_delta_x = 2 - new_x}
    if (new_y < 2) {known_delta_y = 2 - new_y}

    process.maze.forEach(
        (row, y) => row.forEach(
            (cell, x) => result[y + known_delta_y][x + known_delta_x] = cell
        )
    );

    let look_delta_x = new_x - 2 + known_delta_x;
    let look_delta_y = new_y - 2 + known_delta_y;

    look.forEach(
        (row, y) => row.forEach(
            (cell, x) => result[y + look_delta_y][x + look_delta_x] = cell === 'P' ? 'E' : cell
        )
    );

    let detect_wall_length = 5;

    for (let y = 0; y < result.length; y++) {
        let wall_length = 0;

        for (let x = 0; x < result[0].length; x++) {
            if (result[y][x] === 'X') {
                wall_length++;

                if (wall_length === detect_wall_length) {
                    break;
                }
            }
            else {
                wall_length = 0;
            }
        }

        if (wall_length === detect_wall_length) {
            for (let x = 0; x < result[0].length; x++) {
                result[y][x] = 'X';
            }
        }
    }

    for (let x = 0; x < result[0].length; x++) {
        let wall_length = 0;

        for (let y = 0; y < result.length; y++) {
            if (result[y][x] === 'X') {
                wall_length++;

                if (wall_length === detect_wall_length) {
                    break;
                }
            }
            else {
                wall_length = 0;
            }
        }

        if (wall_length === detect_wall_length) {
            for (let y = 0; y < result.length; y++) {
                result[y][x] = 'X';
            }
        }
    }

    process.setMaze(result);

    if (known_delta_x !== 0 || known_delta_y !== 0) {
        let new_routes = {};
        let routes     = process.getAllowedRoutes();

        let setTmpRoute = function(x,y) {
            new_routes[y + known_delta_y] ||= {};
            new_routes[y + known_delta_y][x + known_delta_x] = 1;
        }

        Object.keys(routes).forEach(
            (y) => Object.keys(y).forEach(
                (x) => setTmpRoute(x,y)
            )
        );

        process.setAllowedRoutes(new_routes);
    }

    let useless_points = {};

    for (let y = 0; y < result.length; y++) {
        for (let x = 0; x < result[0].length; x++) {
            let is_point_useless = 1;

            uselessLoop:
            for (let cy = y - 1; cy <= y + 1; cy++) {
                for (let cx = x - 1; cx <= x + 1; cx++) {
                    if (
                        !result[cy]
                        || !result[cy][cx]
                        || result[cy][cx] === "U"
                        || result[cy][cx] === "G"
                    ) {
                        is_point_useless = 0;

                        break uselessLoop;
                    }
                }
            }

            useless_points[y] ||= {};
            useless_points[y][x] = is_point_useless;
        }
    }

    process.useless_points = useless_points;

    process.cur_x   = new_x   + known_delta_x;
    process.cur_y   = new_y   + known_delta_y;
    process.start_x = start_x + known_delta_x;
    process.start_y = start_y + known_delta_y;
}

function getRoute(process, start, end) {
    let maze = process.maze;

    const openList   = [];
    const closedList = [];

    const numRows = maze.length;
    const numCols = maze[0].length;

    const startPosition = new Position(start[0], start[1]);
    const endPosition   = new Position(end[0], end[1]);

    openList.push(startPosition);

    while (openList.length > 0) {
        let currentPosition = openList[0];
        let currentIndex = 0;

        for (let i = 1; i < openList.length; i++) {
            if (openList[i].path_total < currentPosition.path_total) {
                currentPosition = openList[i];
                currentIndex = i;
            }
        }

        openList.splice(currentIndex, 1);
        closedList.push(currentPosition);

        if (currentPosition.x === endPosition.x && currentPosition.y === endPosition.y) {
            const path = [];
            let current = currentPosition;

            while (current !== null) {
                if (current.cmd) {
                    path.push(current.cmd);
                }

                current = current.parent;
            }

            process.setRouteAllowedTo(end[0], end[1]);

            return path.reverse();
        }

        const neighbors = [
            ['LEFT',  currentPosition.x - 1, currentPosition.y    ],
            ['RIGHT', currentPosition.x + 1, currentPosition.y    ],
            ['UP',    currentPosition.x,     currentPosition.y - 1],
            ['DOWN',  currentPosition.x,     currentPosition.y + 1]
        ];

        for (const neighbor of neighbors) {
            const [cmd, neighborX, neighborY] = neighbor;

            if (isPositionAllowed(maze, neighborX, neighborY, numCols, numRows)) {
                const neighborPosition = new Position(neighborX, neighborY, cmd);

                if (!closedList.some(Position => Position.x === neighborPosition.x && Position.y === neighborPosition.y)) {
                    const tentativeG       = currentPosition.path_length + 1;
                    const existingPosition = openList.find(Position => Position.x === neighborPosition.x && Position.y === neighborPosition.y);

                    if (existingPosition === undefined || tentativeG < existingPosition.path_length) {
                        neighborPosition.path_length = tentativeG;
                        neighborPosition.path_left   = Math.abs(neighborX - endPosition.x) + Math.abs(neighborY - endPosition.y);
                        neighborPosition.path_total  = neighborPosition.path_length + neighborPosition.path_left;
                        neighborPosition.parent      = currentPosition;

                        if (existingPosition === undefined) {
                            openList.push(neighborPosition);
                        }
                    }
                }
            }
        }
    }

    return null;
}

function stepPlayer(player, cmd) {
    if (cmd === 'UP') {
        player.up();
    }
    else if (cmd === 'RIGHT') {
        player.right();
    }
    else if (cmd === 'LEFT') {
        player.left();
    }
    else if (cmd === 'DOWN') {
        player.down();
    }
}

function lookAround(player) {
    let look = player.lookAround();

    look.forEach(
        (row) => row.forEach(
            (cell, x) => row[x] ||= 'X'
        )
    );

    return look;
}

function isCloseupCell(x, y, cx, cy, range) {
    return (cy >= y - range || cy <= y + range)
    &&
    (cx >= x - range || cx <= x + range);
}

function isPositionAllowed(maze, x, y, num_cols, num_rows) {
    return (
        x >= 0 && y >= 0 &&
        x < num_cols && y < num_rows &&
        maze[y] && ['E','G','P'].includes(maze[y][x])
    );
}

function sleep() {
    return new Promise(resolve => setTimeout(resolve, 50));
}
