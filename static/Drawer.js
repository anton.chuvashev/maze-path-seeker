function showPath(x, y, road, prefix) {
    showPlayer(x, y, prefix);

    road.forEach(
        (cmd, index) => {
            index && $("#maze-table-td-" + y + "-" + x + prefix).html("*");

                 if (cmd === 'UP')    {y--}
            else if (cmd === 'RIGHT') {x++}
            else if (cmd === 'LEFT')  {x--}
            else if (cmd === 'DOWN')  {y++}
        }
    );

    showExit(x, y, prefix);
}

function redrawPlayer(player, prefix) {
    showPlayer(player.x, player.y, prefix);
}

function redrawKnown(known, start_x, start_y) {
    showMaze(known, '-known');
    showStart(start_x, start_y, '-known');
}

function showPlayer(x, y, prefix) {
    showObject('P', x, y, 'yellow', prefix)
}

function showStart(x, y, prefix) {
    let cleaner = function() {
        $(this).css("border", "collapsed white 0px")
    }

    if (prefix) {
        $("td[id^=maze-table-td-]", "td[id$=" + prefix + "]").each(cleaner);
    }
    else {
        $("td[id^=maze-table-td-]").each(cleaner);
    }

    $("#maze-table-td-" + y + "-" + x + prefix).css("border", "solid black 1px");
}

function showExit(x, y, prefix) {
    showObject('G', x, y, 'red', prefix)
}

function showObject(symbol, x, y, color, prefix) {
    let cleaner = function() {
        if ($(this).html() == symbol) {$(this).css("background-color", "#EEEEEE")}
        if ($(this).html() == symbol) {$(this).html('')}
    }

    if (prefix) {
        $("td[id^=maze-table-td-]", "td[id$=" + prefix + "]").each(cleaner);
    }
    else {
        $("td[id^=maze-table-td-]").each(cleaner);
    }

    $("#maze-table-td-" + y + "-" + x + prefix).css("background-color", color);
    $("#maze-table-td-" + y + "-" + x + prefix).html(symbol);
}

function showMaze(maze, suffix) {
    $('#maze' + suffix).html('<table id="maze-table' + suffix + '"></table>');

    let row_count = 0;

    maze.forEach(
        (row) => addRow(row, row_count++, suffix)
    );
}

function addRow(row, row_count, suffix) {
    $('#maze-table' + suffix).append('<tr id="maze-table-tr-' + row_count + suffix + '"></tr>');

    let cell_count = 0;

    row.forEach(
        (cell) => addCell(cell, row_count, cell_count++, suffix)
    );
}

function addCell(cell, row_count, cell_count, suffix) {
    cell ||= 'U';

    let td_class = 'td-' + cell.toLowerCase();

    $('#maze-table-tr-' + row_count + suffix).append(
        '<td class="' + td_class + '" id="maze-table-td-' + row_count + '-' + cell_count  + suffix + '">&nbsp;</td>'
    );
}
